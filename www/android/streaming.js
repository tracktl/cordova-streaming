var api = (function(){

	var exec = require('cordova/exec');


	var expose = {}



	expose._updateState = function( state ){
		switch( state ){
			case 'ended':
			case 'buffering':
			case 'playing':
		}
	}


	;[
		'init',

		'preload',
		'unload',
		'play',
		'pause',

		'setPosition',
		'setVolume',

		'getPosition',
		'getDuration',
		'getPercentLoaded',

	].forEach( function( method ){
		expose[ method ] = function(){

			var args = [].slice.call( arguments )

			return new Promise( function(resolve, reject){
				exec(
					resolve,
					reject,
					'Streaming',
					method,
				 	args
				)
			})
		}
	})


	return expose
})();






var listeners = [];


api._updateState = function( state ){
	switch( state ){
		case 'ended':
			return notifySetState( states.STOPPED )
		case 'buffering':
			return notifySetState( states.BUFFERING )
		case 'playing':
			return notifySetState( states.PLAYING )
	}
}

var _url
var _inited
var ensureInited = function(){
	if (_inited)
		return _inited === true ? Promise.resolve() : _inited

	return _inited = api.init( )

		.then(function(){
			_inited = true
		})
}

var notifySetState = function (state, url) {
	console.log('--- state chaged : '+state)
	url = url || _url
	if(!_url) return;
	for(var i = 0; i < listeners.length; i++) {
		listeners[i](state, _url);
	}
}

var states = {
	READY : 2,
	PLAYING : 0,
	BUFFERING : 4,
	PAUSED : 1,
	STOPPED : 3,
	ERROR : 5,
}

module.exports = {
	_updateState : api._updateState,
	api : api,
	register : function(callback){
		listeners.push(callback);
		return function(){
			listeners.splice(listeners.indexOf(callback), 1);
		};
	},
	playNewFile : function (url) {

		_url = url

		ensureInited()

		.then( function(){
			return api.preload( url )
		})


		.then( notifySetState.bind( null, states.READY ) )

		.then( function(){
			return api.play( )
		})
		.then( notifySetState.bind( null, states.PLAYING ) )
		.catch( notifySetState.bind( null, states.ERROR ) )
	},
	preload : function (url) {

		var current = _url

		_url = url

		ensureInited()

		.then( function(){
			return api.preload( url )
		})
		.then( notifySetState.bind( null, states.READY ) )


		.then( function(){

			if ( current )
				notifySetState( states.STOPPED, current )
		})

		.catch( notifySetState.bind( null, states.ERROR ) )
	},
	getPosition : function (callback) {
		api.getPosition()
		.then( function(x){
			callback( x/1000 )
		})
	},
	getDuration : function (callback) {
		api.getDuration()
		.then( function(x){
			callback( x/1000 )
		})
	},
	play : function () {
		ensureInited()
		.then( function(){
			return api.play( )
		})
		.then( notifySetState.bind( null, states.PLAYING ) )
		.catch( notifySetState.bind( null, states.ERROR ) )
	},
	pause : function () {
		ensureInited()
		.then( function(){
			return api.pause( )
		})

		//// /!\ nasty hack
		// silently fail
		.catch( function(){} )

		.then( notifySetState.bind( null, states.PAUSED ) )
		.catch( notifySetState.bind( null, states.ERROR ) )
	},
	stop : function () {
		ensureInited()
		.then( function(){
			return api.unload( )
		})
		.then( notifySetState.bind( null, states.PLAYING ) )
		.catch( notifySetState.bind( null, states.ERROR ) )
	},
	STATES : states
};
