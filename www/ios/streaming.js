var exec = require('cordova/exec');

var listeners = [];

module.exports = {
	register : function(callback){
		listeners.push(callback);
		return function(){
			listeners.splice(listeners.indexOf(callback), 1);
		};
	},
	playNewFile : function (url) {
		exec(function(){}, function(){}, "Streaming", "playNewFile", [url]);
	},
	preload : function (url) {
		exec(function(){}, function(){}, "Streaming", "preload", [url]);
	},
	getPosition : function (callback) {
		exec(callback, function(){}, "Streaming", "getPosition", []);
	},
	getDuration : function (callback) {
		exec(callback, function(){}, "Streaming", "getDuration", []);
	},
	play : function () {
		exec(function(){}, function(){}, "Streaming", "play", []);
	},
	pause : function () {
		exec(function(){}, function(){}, "Streaming", "pause", []);
	},
	stop : function () {
		exec(function(){}, function(){}, "Streaming", "stop", []);
	},
	onStatus : function (state, url) {
		console.log("state : " + state)
		console.log(url)
		if(!url) return;
		for(var i = 0; i < listeners.length; i++) {
			listeners[i](state, url);
		}
	},
	STATES : {
		READY : 2,
		//RUNNING : 1,
		PLAYING : 0,
		BUFFERING : 4,
		PAUSED : 1,
		STOPPED : 3,
		ERROR : 5,
		//DISPOSED : 64
	}
};
