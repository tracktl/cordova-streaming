//
//  CDVStreaming.h
//  Tracktl
//
//  Created by Tracktl on 24/06/14.
//
//


#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MPNowPlayingInfoCenter.h>
#import "DOUAudioStreamer.h"
#import "DOUAudioFile.h"
#import "Track.h"


@interface CDVStreaming : CDVPlugin

- (void)getPosition:(CDVInvokedUrlCommand*)command;
- (void)getDuration:(CDVInvokedUrlCommand*)command;
- (void)playNewFile:(CDVInvokedUrlCommand*)command;
- (void)preload:(CDVInvokedUrlCommand*)command;
- (void)play:(CDVInvokedUrlCommand*)command;
- (void)pause:(CDVInvokedUrlCommand*)command;
- (void)stop:(CDVInvokedUrlCommand*)command;
@property DOUAudioStreamer *_streamer;
@property DOUAudioStreamer *_preloading_streamer;

@end
