//
//  CDVStreaming.m
//  Tracktl
//
//  Created by Tracktl on 24/06/14.
//
//

#import "CDVStreaming.h"
#import "MainViewController.h"

static void *kStatusKVOKey = &kStatusKVOKey;


@implementation CDVStreaming


- (void)pluginInitialize
{
    [super pluginInitialize];
    
    // meme pas besoin de gerer les audio sessions, merci DOUAS !

    MainViewController *mainController = (MainViewController*)self.viewController;
    mainController.remoteControlPlugin = self;
    [mainController canBecomeFirstResponder];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    self._streamer = nil;
}

- (void)_cancelStreamer
{
  if (self._streamer != nil) {
    [self._streamer pause];
    [self._streamer removeObserver:self forKeyPath:@"status"];
    self._streamer = nil;
  }
}

- (void)getPosition:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDouble:[self._streamer currentTime]];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getDuration:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDouble:[self._streamer duration]];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)playNewFile:(CDVInvokedUrlCommand*)command
{
    NSURL* url = [NSURL URLWithString:[command.arguments objectAtIndex:0]];
    
    [self _cancelStreamer];
    
    if([self._preloading_streamer.url isEqual:url]) {
        self._streamer = self._preloading_streamer;
    } else {
        Track * afile = [Track alloc];
        afile.audioFileURL = url;
        
        self._streamer = [DOUAudioStreamer streamerWithAudioFile:afile];
    }
    self._preloading_streamer = nil; // on clean de toute facon

    
    [self._streamer addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:kStatusKVOKey];
    [self._streamer play];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if (context == kStatusKVOKey) {
    [self performSelector:@selector(sendStatus)
                 onThread:[NSThread mainThread]
               withObject:nil
            waitUntilDone:NO];
  }
}


- (void)play
{
    [self._streamer play];
}

- (void)play:(CDVInvokedUrlCommand*)command
{
    [self play];
}

- (void)pause
{
    [self._streamer pause];
}

- (void)pause:(CDVInvokedUrlCommand*)command
{
    [self pause];
}


- (void)stop
{
    [self._streamer stop];
}

- (void)stop:(CDVInvokedUrlCommand*)command
{
    [self stop];
}


- (void)preload:(CDVInvokedUrlCommand*)command
{
    NSURL* url = [NSURL URLWithString:[command.arguments objectAtIndex:0]];
    
    Track * afile = [Track alloc];
    afile.audioFileURL = url;
    
    self._preloading_streamer = [DOUAudioStreamer streamerWithAudioFile:afile];
}


// INTERNALS



- (void)_sendStatus:(int)STATE queueId:(NSObject *)queueId
{
    NSString* jsString = [NSString stringWithFormat:@"%@(\"%d\", \"%@\");", @"cordova.require('org.apache.cordova.streaming.streaming').onStatus", STATE, queueId];
    [self.commandDelegate evalJs:jsString];
}


- (void)sendStatus
{
    [self _sendStatus:[self._streamer status] queueId:self._streamer.url];
}
- (void)sendStatus:(int)STATE
{
    [self _sendStatus:STATE queueId:self._streamer.url];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlPlay:
            [self play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self pause];
            break;
        case UIEventSubtypeRemoteControlNextTrack:
        case UIEventSubtypeRemoteControlStop:
            [self sendStatus:DOUAudioStreamerFinished];
            [self stop];
            break;
        default:
            break;
    }
}

- (void)onReset
{
    // si il y a une lecture en cours stop
    [self stop];
}





@end
