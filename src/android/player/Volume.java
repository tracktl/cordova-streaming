package tl.track.application.cordova.streaming.player;


import tl.track.application.cordova.streaming.player.Api;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;


import android.media.MediaPlayer;
import android.util.Log;


public class Volume extends Api {

    public Volume( CordovaWebView webView ){
        super( webView );
    }

    public void setVolume( double value, CallbackContext callback ){

        if (!inited)
            callback.error( "player not inited yet" );



        else {

            try{
                mMediaPlayer.setVolume( (float)value, (float)value );

            } catch (Throwable t) {
                callback.error( t.toString() );
                Log.e(TAG, "when setting volume ", t);
            }

            // finish
            callback.success();
        }
    }
}
