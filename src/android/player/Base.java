package tl.track.application.cordova.streaming.player;

import org.apache.cordova.CordovaWebView;

import android.util.Log;


public class Base {

    public static String TAG = "TracktlPlayer";

    private CordovaWebView webView;

    public Base( CordovaWebView webView_ ){
        webView = webView_;
    }

    protected void updateState(String state) {
    	webView.sendJavascript("window.streaming._updateState(\""+ state +"\");" );
    }
}
