package tl.track.application.cordova.streaming.player;


import tl.track.application.cordova.streaming.player.Loader;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;


import java.io.IOException;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;


public class Api extends Loader {

    protected CallbackContext preloadCallback;


    protected int percentLoaded =0;

    protected boolean trackLoaded = false;


    public Api( CordovaWebView webView ){
        super( webView );
    }

    public void preload( String url, CallbackContext callback ){

        if (!inited)
            callback.error( "player not inited yet" );


        else {

            // hold this value
            preloadCallback = callback;

            // reset this value
            percentLoaded = 0;

            try {

                trackLoaded = false;

                // stop the previous player
                mMediaPlayer.reset();

                // set the source
                mMediaPlayer.setDataSource( url );
                mMediaPlayer.setAudioStreamType( AudioManager.STREAM_MUSIC );

                // prepare the event handlers
                mMediaPlayer.setOnPreparedListener( onTrackPrepared );
                mMediaPlayer.setOnCompletionListener( onTrackCompleted );
                mMediaPlayer.setOnBufferingUpdateListener( onBuffered );
                mMediaPlayer.setOnErrorListener( errorListener );

                // prepare the player
                Log.d(TAG, "prepare track");
                mMediaPlayer.prepareAsync();

            } catch (Throwable t) {
                Log.e(TAG, "error when loading the track", t);
                callback.error( t.toString() );

                mMediaPlayer.reset();
            }
        }
    }

    MediaPlayer.OnPreparedListener onTrackPrepared = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            Log.d(TAG, "track prepared");
            trackLoaded = true;
            if ( preloadCallback != null ) {
                preloadCallback.success();
                preloadCallback = null;
            }
        }
    };

    MediaPlayer.OnErrorListener errorListener = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            Log.d(TAG, "streaming err");
            if ( preloadCallback != null ) {
                preloadCallback.error( "what "+what+" extra "+extra );
                preloadCallback = null;
            }
            return false;
       }
    };

    public void unload( CallbackContext callback ){

        if (!inited){
            Log.e(TAG, "player not inited yet receive a unload ");
            callback.error( "player not inited yet" );
        }

        else if (!trackLoaded){
            Log.e(TAG, "track is not ready yet receive a unload ");
            callback.success();
        }

        else {
            trackLoaded = false;

            // stop the previous player
            mMediaPlayer.stop();
            mMediaPlayer.reset();


            // finish
            callback.success();
        }
    }


    public void play( CallbackContext callback ) {

        if (!inited){
            Log.e(TAG, "player not inited yet receive a play ");
            callback.error( "player not inited yet" );
        }

        else if (!trackLoaded){
            Log.e(TAG, "track is not ready yet receive a play ");
            callback.error( "track is not ready yet" );

        }

        else{
            try{
                mMediaPlayer.start();
                callback.success();
            } catch (Throwable t) {
                callback.error( t.toString() );
                Log.e(TAG, "when start playing ", t);
            }
        }
    }

    public void pause( CallbackContext callback ) {

        if (!inited){
            Log.e(TAG, "player not inited yet receive a pause ");
            callback.error( "player not inited yet" );
        }

        else if (!trackLoaded){
            Log.e(TAG, "track is not ready yet receive a pause ");
            callback.error( "track is not ready yet" );

        }

        else{
            try{
                mMediaPlayer.pause();
                callback.success();
            } catch (Throwable t) {
                callback.error( t.toString() );
                Log.e(TAG, "when pausing ", t);
            }
        }
    }




    MediaPlayer.OnCompletionListener onTrackCompleted = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            Log.d(TAG, "streaming over");
            updateState( "ended" );
        }
    };


    public void setPosition( double value, CallbackContext callback ){

        if (!inited)
            callback.error( "player not inited yet" );

        else if (!trackLoaded)
            callback.error( "track is not ready yet" );

        else {
            try{
                mMediaPlayer.seekTo( (int)value );
                callback.success();
            }catch( Throwable t ){
                callback.error( t.toString() );
                Log.e(TAG, "when set Position ", t);
            }
        }
    }

    public int getPosition( ){

        if (!inited)
            return 0;

        else if (!trackLoaded)
            return 0;

        try{
            return mMediaPlayer.getCurrentPosition();
        }catch( Throwable t ){
            return 0;
        }
    }
    public int getDuration( ){

        if (!inited)
            return 0;

        else if (!trackLoaded)
            return 0;

        try{
            return mMediaPlayer.getDuration();
        }catch( Throwable t ){
            return 0;
        }
    }
    public int getPercentLoaded( ){

        if (!inited)
            return 0;

        else if (!trackLoaded)
            return 0;

        // nasty hack
        //   for some reason the listener gets 100 buffered before the track is playing ( android lolipop )
        try{
            if( mMediaPlayer.getCurrentPosition() > 0 )
                return percentLoaded;
            else
                return 0;
        }catch( Throwable t ){
            return 0;
        }
    }

    MediaPlayer.OnBufferingUpdateListener onBuffered = new MediaPlayer.OnBufferingUpdateListener() {
        @Override
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            Log.v(TAG, "on buffered event, percent loaded " + percentLoaded);
            percentLoaded = percent;
        }
    };
}
