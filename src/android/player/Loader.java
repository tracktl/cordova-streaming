package tl.track.application.cordova.streaming.player;


import tl.track.application.cordova.streaming.player.Base;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;

import java.io.IOException;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;


public class Loader extends Base {

    protected MediaPlayer mMediaPlayer = null;
    protected boolean inited = false;

    public Loader( CordovaWebView webView ){
        super( webView );
    }

    public void init( CallbackContext callback ) {

        Log.d(TAG, "initializing the media player");

        try {
            mMediaPlayer = new MediaPlayer();

        } catch (Throwable t) {
            callback.error( t.toString() );
            Log.e(TAG, "when initing the media player", t);
        }

        Log.d(TAG, "media player inited");

        inited = true;

        callback.success();
    }

}
