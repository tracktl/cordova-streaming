package tl.track.application.cordova.streaming;


import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

import tl.track.application.cordova.streaming.player.Player;

import android.util.Log;



public class Streaming extends CordovaPlugin {
    public static final String TAG = "TracktlStreaming";


    private Player player = null;

    /**
     * Constructor.
     */
    public Streaming() {}

    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);

        player = new Player( webView );

        Log.d(TAG, "initializing cordova plugin");
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action            The action to execute.
     * @param args              JSONArry of arguments for the plugin.
     * @param callbackContext   The callback id used when calling back into JavaScript.
     * @return                  True if the action was valid, false if not.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

        Log.v(TAG, "receive the action "+action);

        if( action.equals("init") ){

            player.init( callbackContext );

        } else
        if( action.equals("preload") ){

            player.preload( args.getString(0), callbackContext );

        } else
        if( action.equals("unload") ){

            player.unload( callbackContext );

        } else
        if( action.equals("play") ){

            player.play( callbackContext );

        } else
        if( action.equals("pause") ){

            player.pause( callbackContext );

        } else
        if( action.equals("setPosition") ){

            player.setPosition( args.getDouble(0), callbackContext );

        } else
        if( action.equals("getPosition") ){

            callbackContext.success(
                player.getPosition( ));

        } else
        if( action.equals("setVolume") ){

            player.setVolume( args.getDouble(0), callbackContext );

        } else
        if( action.equals("getDuration") ){

            callbackContext.success(
                player.getDuration( ));

        } else
        if( action.equals("getPercentLoaded") ){

            callbackContext.success(
                player.getPercentLoaded( ));

        } else {
            Log.d(TAG, "no resolution for the action "+action);
            return false;
        }


        return true;
    }

}
